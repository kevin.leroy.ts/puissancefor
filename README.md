# PuissanceFOR

## Cloning Depository

```
git clone https://gitlab.com/kevin.leroy.ts/puissancefor.git
cd ./PuissanceFOR
```

## Install Dependencies

First of all we need to install dependencies, run in terminal:

```
npm install
```

## Quick Start (run development server)

To start the development server and test web app, run in terminal:

```
npm run start
```

## Capacitor (build for android)

This project created with Capacitor support. And first thing required before start is to add capacitor platforms, run in terminal:

```
npx cap add android
npm run build-capacitor-android
npx cap open android
```

Check out [official Capacitor documentation](https://capacitorjs.com) for more examples and usage examples.

## Documentation & Resources

* [Framework7 Core Documentation](https://framework7.io/docs/)

* [Framework7 React Documentation](https://framework7.io/react/)

* [Framework7 Icons Reference](https://framework7.io/icons/)
* [Community Forum](https://forum.framework7.io)