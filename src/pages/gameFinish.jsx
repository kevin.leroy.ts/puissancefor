import React, { useState, useEffect } from 'react';
import { getStorage, setStorage } from '../js/useStorage';

import {
    Page,
    Button,
    Block,
    Row,
    Col
} from 'framework7-react';

import {
    FacebookShareButton,
    FacebookIcon,
    TwitterShareButton,
    TwitterIcon
  } from "react-share";

import Header from '../components/header';
import { takePicture } from '../js/picture';

const GameFinish = ({gagnant}) => {

    const [picture, setPicture] = useState(null);

    useEffect(() => { 
        getStorage('history', [])
        .then((actual_history) => {
            const event = new Date(Date.now());
            if (gagnant === 1) {
                actual_history.push({date: event.toLocaleDateString('fr-FR') + ' - ' + event.toLocaleTimeString('fr-FR'), result: "Gagnée"})
            } else if (gagnant === 0) {
                actual_history.push({date: event.toLocaleDateString('fr-FR') + ' - ' + event.toLocaleTimeString('fr-FR'), result: "Egalité"})
            } else {
                actual_history.push({date: event.toLocaleDateString('fr-FR') + ' - ' + event.toLocaleTimeString('fr-FR'), result: "Perdue"})
            }

            if (actual_history.length > 5) {
                actual_history.shift()
            }

            setStorage('history', actual_history)
        })
    }, []);

    return(
        <Page>
            <Header />
            <div className="finish-page">
                <Block>
                    {
                        gagnant===0 ? <Row>Egalité</Row> :
                        gagnant===1 ? <Row>Vous avez gagné ... Bravo !</Row> :
                        <Row>Vous avez perdu ... Dommage !</Row>
                    }
                    <Row className="mt-4">
                        {picture ? (
                            <img className="finish-img" src={picture} alt="" />
                        ) : (
                            <Button className="m-4" outline round onClick={() => takePicture(setPicture)}>
                                <i className="fa fa-camera" />
                            </Button>
                        )}
                    </Row>

                    <Row><Button className="m-4" outline round href="/play">Nouvelle partie</Button></Row>
                    <Row><Button outline round href="/">Menu principal</Button></Row>
                    
                    {
                        gagnant===1 ? (
                        <Row className="mt-4 no-gap">
                            <Col></Col>
                            <Col>
                                <FacebookShareButton
                                    quote={"J'ai gagné une partie de PuissanceFOR !"}
                                    url={'http://puissancefor.com'}
                                    className=""
                                >
                                    <FacebookIcon size={32} round />
                                </FacebookShareButton>
                            </Col>
                            <Col>
                                <TwitterShareButton
                                    url={"J'ai gagné une partie de PuissanceFOR !"}
                                    className=""
                                >
                                    <TwitterIcon size={32} round />
                                </TwitterShareButton>
                            </Col>
                            <Col></Col>
                        </Row>
                        ) : ""
                    }
                </Block>
            </div>
        </Page>
    );
}

export default GameFinish;
