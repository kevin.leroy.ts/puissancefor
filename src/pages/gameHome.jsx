import React from 'react';

import {
    Page,
    Button,
    Block,
    Row
} from 'framework7-react';

import Header from '../components/header';
import GameHistory from '../components/gameHistory';

const GameHome = ({ f7router }) => {
    return(
        <Page>
            <Header />
            <div className="finish-page">
                <Block>
                    <Row>
                        <Button 
                            outline round 
                            onClick={() => f7router.navigate('/play')}>
                            Lancer une partie
                        </Button>
                    </Row>
                    <Row>
                        <GameHistory />
                    </Row>
                </Block>
            </div>
        </Page>
    );
}

export default GameHome;