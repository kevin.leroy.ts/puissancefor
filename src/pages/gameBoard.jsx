import React, { useState, useEffect } from 'react';

import {
    Page,
    Block,
    Row,
    Col,
    Icon
} from 'framework7-react';

import Header from '../components/header';
import Jeton from '../components/jeton';

import { checkVictoire, getColPlayable } from '../js/playSystem';

const GameBoard = ({ f7router }) => {
    const [grid, setGrid] = useState(
		[
			[0,0,0,0,0,0], // Col 7
			[0,0,0,0,0,0], // Col 6
			[0,0,0,0,0,0], // Col 5
			[0,0,0,0,0,0], // Col 4
			[0,0,0,0,0,0], // Col 3
			[0,0,0,0,0,0], // Col 2
			[0,0,0,0,0,0]  // Col 1
		]
	);

	/*
		True si c'est le tour de l'adversaire
		False sinon
	*/
	const [player_2_tour, setPlayerTour] = useState(false);

	const play = async (col_id) => {
		let new_board = grid

		if (player_2_tour) {
			const col_playable = getColPlayable(grid)
			col_id = col_playable[Math.floor(Math.random()*col_playable.length)]
			await new Promise(r => setTimeout(r, 2000))
		}

		const played_col = new_board[col_id]

		for (let i = played_col.length-1; i >= 0; i--) {
			if (new_board[col_id][i] == 0) {
				player_2_tour ?
				new_board[col_id][i] = 2 :
				new_board[col_id][i] = 1
				break
			}
		}
	
		checkVictoire(f7router, new_board)
	
		setGrid(new_board)
		setPlayerTour(!player_2_tour)
	};

	// Le bot joue
	useEffect(() => {
		if (player_2_tour) {play()}
	}, [player_2_tour])

    return(
        <Page>
            <Header />
			{
				player_2_tour ?
				<h5 className="mt-3">L'adversaire est en train de jouer ...</h5> :
				<h5 className="mt-3">C'est votre tour !</h5>
			}
            <Block className="grid">

                {grid.map((col, col_idx) => 
					<Row key={col_idx} noGap>
						<Col className="arrow_container">
							<Icon f7="arrowtriangle_right_fill"></Icon>
						</Col>

						{col.map((y, row_idx) => 
							<Col key={row_idx}>
								<Jeton 
									val={y} 
									col={col_idx} 
									clickFct={
										(getColPlayable(grid).includes(col_idx) && !player_2_tour) ? (
											() => play(col_idx, false)
										) : (
											() => null
										)
									} 
								/>
							</Col>
						)}
					</Row>
				)}

            </Block>
        </Page>
    )
}

export default GameBoard;
