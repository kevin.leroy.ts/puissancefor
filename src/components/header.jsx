import React from 'react';

const Header = () => {
    return(
        <div className="App-header">
            <h1 className="text-header">
                <span>"Puissance</span><span>For"</span>
            </h1>
        </div>
    );
}

export default Header;
