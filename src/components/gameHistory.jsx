import React, {useState, useEffect} from 'react';
import { getStorage } from '../js/useStorage';

import {
    Block
} from 'framework7-react';

const GameHistory = () => {
    const [history, setHistory] = useState(null);

    useEffect(() => { 
        getStorage('history').then((actual_history) => setHistory(actual_history));  
    },[]);

    return(
        <Block className="m-5">
        {history ?
        <>
        <h4>Vos 5 dernières parties</h4>
        <div className="data-table aurora">
            <table>
                <tbody>
                {history.reverse().map((game, game_idx) => 
                    <tr key={game_idx}>
                        <td>{game.date}</td> 
                        <td>{game.result}</td>
                    </tr>
                )}
                </tbody>
            </table>
        </div></> :
        ""}
        </Block>
    );
}

export default GameHistory;
