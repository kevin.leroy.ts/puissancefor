import React from 'react';

const Jeton = ({col, val, clickFct}) => {
    return(
        <div 
            className={`jeton 
                ${(val == 2) ? "jaune" : (val == 1) ? "rouge" : "gris"}`} 
            onClick={clickFct ? () => clickFct(col) : null} >
        </div>
    );
}

export default Jeton;
