import React, { useState } from 'react';
import { getDevice }  from 'framework7/lite-bundle';
import {
  f7,
  f7ready,
  App,
  View
} from 'framework7-react';

import capacitorApp from '../js/capacitor-app';
import routes from '../js/routes';
import store from '../js/store';

const PuissanceFOR = () => {
	const device = getDevice();

	// Framework7 Parameters
	const f7params = {
		name: 'PuissanceFOR', // App name
		theme: 'auto', // Automatic theme detection
		id: 'fr.ensai.puissancefor', // App bundle ID
		// App store
		store: store,
		// App routes
		routes: routes,
		// Register service worker (only on production build)
		serviceWorker: process.env.NODE_ENV ==='production' ? {
			path: '/service-worker.js',
		} : {},
		// Input settings
		input: {
			scrollIntoViewOnFocus: device.capacitor,
			scrollIntoViewCentered: device.capacitor,
		},
		// Capacitor Statusbar settings
		statusbar: {
			iosOverlaysWebView: true,
			androidOverlaysWebView: false,
		},
	};

	f7ready(() => {
		if (f7.device.capacitor) {
			capacitorApp.init(f7);
		}
		// Call F7 APIs here
	});

	return (
		<App className="App" { ...f7params } >
			<View main url="/" />
		</App>
	)
}
export default PuissanceFOR;