import NotFoundPage from '../pages/404.jsx';
import GameHome from '../pages/gameHome';
import GameBoard from '../pages/gameBoard';
import GameFinish from '../pages/gameFinish';

var routes = [
  {
    path: '/',
    component: GameHome,
  },
  {
    path: '/play',
    component: GameBoard,
  },
  {
    path: '/finish',
    component: GameFinish,
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
