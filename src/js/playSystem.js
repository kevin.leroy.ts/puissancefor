export const checkVictoire = (f7router, new_board) => {
    let gagnant = 0;

    // Victoire en colonne
    for (let col = 0; col < new_board.length; col++) {
        for (let ln = 0; ln < new_board[col].length-3; ln++) {
            if (new_board[col][ln] === 1 && new_board[col][ln+1] === 1 && new_board[col][ln+2] === 1 && new_board[col][ln+3] === 1) {
                gagnant = 1
            } else if (new_board[col][ln] === 2 && new_board[col][ln+1] === 2 && new_board[col][ln+2] === 2 && new_board[col][ln+3] === 2) {
                gagnant = 2
            }
        }
    }

    // Victoire en ligne
    for (let col = 0; col < new_board.length-3; col++) {
        for (let ln = 0; ln < new_board[col].length; ln++) {
            if (new_board[col][ln] === 1 && new_board[col+1][ln] === 1 && new_board[col+2][ln] === 1 && new_board[col+3][ln] === 1) {
                gagnant = 1
            } else if (new_board[col][ln] === 2 && new_board[col+1][ln] === 2 && new_board[col+2][ln] === 2 && new_board[col+3][ln] === 2) {
                gagnant = 2
            }
        }
    }

    // Victoire diagonale montante
    for (let col = 0; col < new_board.length-3; col++) {
        for (let ln = 0; ln < new_board[col].length-3; ln++) {
            if (new_board[col][ln] === 1 && new_board[col+1][ln+1] === 1 && new_board[col+2][ln+2] === 1 && new_board[col+3][ln+3] === 1) {
                gagnant = 1
            } else if (new_board[col][ln] === 2 && new_board[col+1][ln+1] === 2 && new_board[col+2][ln+2] === 2 && new_board[col+3][ln+3] === 2) {
                gagnant = 2
            }
        }
    }

     // Victoire diagonale descendante
    for (let col = 0; col < new_board.length-3; col++) {
        for (let ln = new_board[col].length; ln > 2; ln--) {
            if (new_board[col][ln] === 1 && new_board[col+1][ln-1] === 1 && new_board[col+2][ln-2] === 1 && new_board[col+3][ln-3] === 1) {
                gagnant = 1
            } else if (new_board[col][ln] === 2 && new_board[col+1][ln-1] === 2 && new_board[col+2][ln-2] === 2 && new_board[col+3][ln-3] === 2) {
                gagnant = 2
            }
        }
    }

    // Egalite
    if (gagnant === 0) {
        for (let col = 0; col < new_board.length; col++) {
            for (let ln = 0; ln < new_board[col].length; ln++) {
                if (new_board[col][ln] === 0) {
                    gagnant = null
                }
            }
        }
    }

    gagnant!=null ? f7router.navigate('/finish', {props: {gagnant: gagnant, grid: new_board}}) : null
}


export const getColPlayable = (board) => {
    let col_playable = [];

    // Victoire en colonne
    for (let col = 0; col < board.length; col++){
        for (let ln = 0; ln < board[col].length; ln++) {
            if (board[col][ln] === 0) {
                col_playable.push(col)
                break
            }
        }
    }

    return col_playable
}