import { Storage } from '@capacitor/storage';

const APP_NAME = 'fr.ensai.puissancefor';

export const getStorage = async(key, defaultValue) => {
    const { value } = await Storage.get({
        key: `${APP_NAME}.${key}`
    });

    let returnValue;
    try {
        if (value) {
            returnValue = JSON.parse(value);
        };
    } catch(e) {
        returnValue = value;
    };
    return returnValue || defaultValue;
};

export const setStorage = async(key, value) => {
    let newValue;
    try {
        newValue = JSON.stringify(value);
    } catch(e) {
        newValue = value;
    };
    Storage.set({
        key: `${APP_NAME}.${key}`,
        value: newValue
    });
};