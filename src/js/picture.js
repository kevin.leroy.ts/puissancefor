import { Camera, CameraResultType } from '@capacitor/camera';

export const takePicture = async (setPicture) => {
    const image = await Camera.getPhoto({
        quality: 90,
        allowEditing: false,
        saveToGallery: false,
        source: 'CAMERA',
        resultType: CameraResultType.Uri
    });

    var imageUrl = image.webPath;

    setPicture(imageUrl);
};